<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

    $router->get('banner','ListController@listBanner');
    $router->get('looks','ListController@listLooks');
    $router->get('newarr','ListController@listNewarr');
    $router->get('kategori','ListController@listKategori');
    $router->get('kategoridetail/{kategori_id}','ListController@listKategoridetail');
    $router->get('media','ListController@listMedia');
    $router->get('mediadetail/{media_id}','ListController@listMediadetail');
    $router->get('about','ListController@listAbout');

	
