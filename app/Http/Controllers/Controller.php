<?php

namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Banner;

class Controller extends BaseController
{
    protected function respondOuput($httpCode,$success,$stateCode,$message,$data)
    {
        $resp = array(
            'http_status_code'=>$httpCode,
            'success'=>$success,
            'state_code'=>$stateCode,
            'message'=>$message,
            'data'=>$data
        );

        return response()->json($resp, $httpCode);
    }

    protected function respondOuputData($httpCode,$success,$stateCode,$message,$data,$page,$pageLimit,$totalPage,$totalItem)
    {
        $resp = array(
            'http_status_code'=>$httpCode,
            'success'=>$success,
            'state_code'=>$stateCode,
            'message'=>$message,
            'data'=>$data,
            'page'=>$page,
            'page_limit'=>$pageLimit,
            'total_page'=>$totalPage,
            'total_item'=>$totalItem
        );

        return response()->json($resp, $httpCode);
    }
    
    
}
