<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function getDataByNik($nik)
    {
        if($nik=="") return $this->respondOuput(400,false,"INVALID_INPUT","parameter(s) is missing",array());

        $data = User::find($nik);
        if(empty($data)) return $this->respondOuput(400,false,"NOT_FOUND","Data tidak ditemukan",array());

        $err = "";
        switch ($data->status) {
            case 'Active':
                $err = "Silakan login, untuk masuk aplikasi";
                break;
            case 'Registered':
                $err = "Akun anda sudah terdaftar, silakan login";
                break;
            case 'Suspended':
                $err = "Akun anda di suspend, silakan hubungi admin / cs / desa setempat";
                break;
            default:
                break;
        }

        if($err != "") return $this->respondOuput(401,false,"INVALID_INPUT",$err,array());

        $username = ($data->username=="")?$this->getUsername($data->nama_lengkap_penduduk):$data->username;
        $result = array(
            "nama_lengkap"  => $data->nama_lengkap_penduduk,
            "nik"           => $data->nik,
            "provinsi"      => $data->desa->kecamatan->kabupaten->provinsi->nama,
            "kabupaten"     => $data->desa->kecamatan->kabupaten->nama,
            "kecamatan"     => $data->desa->kecamatan->nama,
            "desa"          => $data->desa->nama_desa,
            "username"      => $username,
        );

        if($data->username == ""){
            $data->username = $username;
            if(!$data->save()) return $this->respondOuput(500,false,"INTERNAL_SERVER_ERROR","Internal Server Error",array());
        }

        return $this->respondOuput(200,true,"","success",$result);
    }

    protected static function getUsername($param)
    {
        $splitName = explode(" ",$param);
        if(count($splitName) <= 1){
            $name = $param;
        }else{
            $name = $splitName[0].end($splitName);
        }

        $permitted_chars = '0123456789';

        $isTrue = false;
        $a = 0;
        while (!$isTrue) {
            if($a < 1){
                $paramName = strtolower($name);
            }else{
                $paramName = strtolower($name).substr(str_shuffle($permitted_chars), 0, 3);
            }

            $getData = User::where('username',$paramName)->first();
            if(empty($getData)) return $paramName;

            $a++;

        }
    }

    public function updateData(Request $request)
    {

        $validate = Validator::make($request->all(),[
            'nik' => 'required',
            'telepon' => 'required|unique:tb_data_penduduk,nomor_telepon,' . $request->nik.',nik',
            'email' => 'required|unique:tb_data_penduduk,email,' . $request->nik.',nik',
            'password' => 'required',
			'foto_ktp' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
			'foto_diri_ktp' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
		]);

        if ($validate->fails()) {
            $messages = $validate->errors();
            return $this->respondOuput(400,false,"INVALID_INPUT",$messages->first(),array());
        }

        $data = User::find($request->nik);
        if(empty($data)) return $this->respondOuput(404,false,"DATA_NOT_FOUND","Data not found",array());

        $err = "";
        switch ($data->status) {
            case 'Active':
                $err = "Silakan login, untuk masuk aplikasi";
                break;
            case 'Registered':
                $err = "Akun anda sudah terdaftar, silakan login";
                break;
            case 'Suspended':
                $err = "Akun anda di suspend, silakan hubungi admin / cs / desa setempat";
                break;
            default:
                break;
        }

        if($err != "") return $this->respondOuput(401,false,"INVALID_INPUT",$err,array());

        $fileKtp = $request->file('foto_ktp');
		$nama_fileKtp = $request->nik.'-'.$fileKtp->getClientOriginalName();
        Storage::disk('custom-ftp')->put('foto_ktp/'.$nama_fileKtp, fopen($fileKtp, 'r+'));

        $file = $request->file('foto_diri_ktp');
		$nama_file = $request->nik.'-'.$file->getClientOriginalName();
        Storage::disk('custom-ftp')->put('foto_selfie/'.$nama_file, fopen($file, 'r+'));

        $data->nomor_telepon = $request->telepon;
        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        $data->foto_selfie = $nama_file;
        $data->foto_ktp = $nama_fileKtp;
        if(!$data->save()) return $this->respondOuput(500,false,"INTERNAL_SERVER_ERROR","Internal Server Error",array());

        return $this->respondOuput(200,true,"","success",array());
    }

    public function login(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'username' => 'required',
            'password' => 'required',
		]);

        if ($validate->fails()) {
            $messages = $validate->errors();
            return $this->respondOuput(400,false,"INVALID_INPUT",$messages->first(),array());
        }

        $getData = User::where('nik',$request->username)->orWhere('username',$request->username)->orWhere('email',$request->username)->first();
        if(empty($getData)) return $this->respondOuput(401,false,"INVALID_INPUT","Data anda Belum terdaftar di sistem kami",array());

        $err = "";
        switch ($getData->status) {
            case 'Listed':
                $err = "Silakan registrasi, untuk masuk aplikasi";
                break;
            case 'Registered':
                $err = "Akun anda dalam tahap registrasi, silakan menunggu";
                break;
            case 'Suspended':
                $err = "Akun anda di suspend, silakan hubungi admin / cs / desa setempat";
                break;
            default:
                break;
        }

        if($err != "") return $this->respondOuput(401,false,"INVALID_INPUT",$err,array());

        $credentials = ['nik' => $getData->nik,'password' => $request->password];
        if (! $token = Auth::attempt($credentials))  {
            return $this->respondOuput(401,false,"INVALID_INPUT","user dan kata sandi anda salah",array());
        }

        $user = User::where('nik',Auth::User()->nik)->first();
        $user->desa = $user->desa->kecamatan->kabupaten->provinsi;
        $data = array(
            'data'=>$user,
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * env('JWT_TTL'));

        return $this->respondOuput(200,true,"","success",$data);
    }

    public function logout() {
        Auth::logout();
        return $this->respondOuput(200,true,"","User successfully signed out",array());
    }

    public function refresh() {
        $user = User::where('nik',Auth::User()->nik)->first();
        $user->desa = $user->desa->kecamatan->kabupaten->provinsi;
        $data = array(
            'data'=>$user,
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * env('JWT_TTL'));

        return $this->respondOuput(200,true,"","success",$data);
    }
}
