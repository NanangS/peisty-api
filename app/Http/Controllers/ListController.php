<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Banner;
use App\Looks;
use App\Newarr;
use App\Kategori;
use App\Kategoridetail;
use App\Media;
use App\About;


class ListController extends Controller
{
    
    public function listBanner()
    {
                $banner = Banner::get();

        return $this->respondOuput(200,true,"","success",$banner);
    }

    public function listLooks()
    {
                $looks = Looks::orderBy('file', 'desc')->take(2)->get();

        return $this->respondOuput(200,true,"","success",$looks);
    }

    public function listNewarr()
    {
                $newarr = Newarr::select(
                            "Newarr.id",
                            "Newarr.category_product_id", 
                            "Newarr.product",
                            "Newarr.price", 
                            "Newarr.description", 
                            "countries.name as country_name"
                        )

                        ->join("product_images", "Newarr.id", "=", "users.country_id")

                        ->get();

        return $this->respondOuput(200,true,"","success",$newarr);
    }

    public function listKategori()
    {
                $kategori = Kategori::get();

        return $this->respondOuput(200,true,"","success",$kategori);
    }

    public function listKategoridetail($kategori_id)
    {           
                $this->title = $kategori_id;
                $this->attributes['slug'] = str_slug($this->title  , "-");
                $kategoridetail = Kategoridetail::where('deskripsi', $slug)->get();

        return $this->respondOuput(200,true,"","success",$kategoridetail);
    }

    public function listMedia()
    {
                $media = Media::orderBy('file', 'desc')->take(4)->get();

        return $this->respondOuput(200,true,"","success",$media);
    }

    public function listAbout()
    {
                $about = About::get();

        return $this->respondOuput(200,true,"","success",$about);
    }
}
