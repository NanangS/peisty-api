<?php

namespace App\Helper;

class StringHelper
{
	public static function encryptString($string) {
		$output = false;

        $secret_key     = env('ENCRYPTION_KEY');
        $secret_iv      = env('IV');
        $encrypt_method = env('ENCRYPTION_MECHANISM');

        // hash
        $key    = hash("sha256", $secret_key);

        // iv – encrypt method AES-256-CBC expects 16 bytes – else you will get a warning
        $iv     = substr(hash("sha256", $secret_iv), 0, 16);

        //do the encryption given text/string/number
        $result = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($result);
        return $output;
	}

	public static function decryptString($string)
	{
		$output = false;

        $secret_key     = env('ENCRYPTION_KEY');
        $secret_iv      = env('IV');
        $encrypt_method = env('ENCRYPTION_MECHANISM');

        // hash
        $key    = hash("sha256", $secret_key);

        // iv – encrypt method AES-256-CBC expects 16 bytes – else you will get a warning
        $iv = substr(hash("sha256", $secret_iv), 0, 16);

        //do the decryption given text/string/number

        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;

	}
}
